Bienvenue sur ma page personnelle du cours FabZero!

Cette page a pour objectif de présenter mon travail effectué dans le cadre de ce cours. En documentant les différentes étapes réalisées et les erreurs commises. Plus précisément, ce site contient de la documentation sur:

- Les 5 modules abordés:

    1. Gestion de projet et documentation

    2. Conception Assistée par Ordinateur

    3. Impression 3D

    4. Outil Fab Lab sélectionné

    5. Arbre de problème et Formation des groupes

- Le projet final

## Qui suis-je ?

Je m'appelle Eliot. Je suis quelqu'un de positif et empathique, je suis d'humeur plutôt calme et posé en général. J'aime découvrir des choses originales et j'adore les gens créatifs qui fusent d'idées incongrues. J'admire les personnes qui ont des fortes passions et qui prennent des initiatives de projet qui portent sur les choses qui les inspirent et les motivent. Mon idéal est de tendre vers ce genre de personne.

Je suis quelqu'un de très rationnel et qui aime la simplicité. Je trouve aussi que j'ai du mal à prendre du recul, j'ai tendance à avoir mes yeux rivés sur l'instant sans avoir de vision globale.

J'ai 22 ans et je suis en dernière année d'étude pour devenir ingénieur civil physicien, la physique me passionne et j'ai choisi pour ma dernière année de me spécialiser dans la physique quantique. Mon mémoire porte d'ailleurs sur la causalité quantique. 

![champEcosse](fabzero-modules/images/ChampEcosse.jpg)

J'aime faire des blagues, en voici une : **Mes poissons se disputent dans mon aquarium, le thon monte vraiment.**


Voici certaines choses qui me passionnent :

- ***La physique*** C'est vraiment une dinguerie ce truc. Quand tu crois avoir compris un phénomène physique ***BIM*** retournement de situation, en fait, il y a un autre niveau de profondeur qui explique le phénomène plus en détail. Au plus tu creuses dans la physique et au plus tu découvres des modèles avec des complexités totalement abusées qui possèdent des propriétés exotiques que tu n'aurais jamais pu imaginer. On dirait un film de braquage avec des retournements de situation stupéfiants toutes les 20 minutes et tu as un nouveau film de la franchise qui sort tous les mois depuis la nuit des temps.
C'est la création et l'utilisation de ces modèles qui sont à l'origine du triomphe technologique de l'humanité, mais de manière surprenante, la physique n'a jamais donné aucun indice sur les raisons de l'existence de notre monde tel qu'il est et pourquoi il obéit partiellement à des modèles aussi étranges.
J'ai fait un site dont l'objectif est d'expliquer certains concepts de physique de la manière la plus simple et directe possible, il s'appelle [Physics Ocean](http://www.physicsocean.org/).

- ***L'escalade*** C'est super addictif, tu as un obstacle, tu le grimpes, tu tombes, tu regrimpes, tu atteins le sommet, tu es content, tu choisis un nouvel obstacle...
Voici une photo de moi et [Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/)
en train de grimper à Fontainebleau. C'est une super belle forêt au sud de Paris avec pleins de chouettes rochers à escalader.

![Fontainebleau](fabzero-modules/images/Fontainebleau.jpg)

- ***Les blagues***. En voici une autre : **T'as une jolie descente hein. J'aurais pas envie de la remonter à vélo.**

- ***Le dessin et la peinture*** Je ne suis pas très bon, mais je me suis inscrit à des cours du soir cette année pour rectifier ça ! Mon peintre préféré est Caspar David Friedrich, j'aime aussi les dessins de Moebius et je suis super fan des animations de [Reindre](https://reindre.org/). 

- ***Mes amis et ma famille***

- ***La musique*** Je joue principalement de la guitare et de la basse, tu peux m'écouter sur ma [chaine youtube](https://www.youtube.com/watch?v=k6L14hTDAWg). Sinon j'écoute beaucoup de genres différents, mes artistes préférés qui me viennent en tête sont Algernon Cadwallader, Jamiroquai, Népal, Saba, Tyler The Creator.
Voici une [vidéo](https://www.youtube.com/watch?v=AsI9yHNbFJU) de moi en train de faire un peu de guitare à un concert avec [Martin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/) qui fait les dédicaces, [Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/) à la caméra, [Emma](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/emma.dubois/) au cheerleading, [Jean-François](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jean-francois.loumeau/) quelque part dans le public et un big ami à moi Oliver au chant! Je vous love tous!
Denis on te dédie cette vidéo, plein de love sur toi, merci pour ton cours, merci pour avoir permis de faire toutes ces chouettes rencontres <3