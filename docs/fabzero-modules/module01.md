# 1. Gestion de projet et documentation

Le premier module du cours FabZero a pour objectif d'apprendre à utiliser GitLab et Markdown.
Cela met ainsi à disposition des outils **puissants** pour réaliser la documentation du cours.


Je ne suis pas familier avec GitLab, GitHub et Markdown. La première étape pour moi est donc d'installer Git Bash. 

## Installation de Git Bash

Selon [Atlassian](https://www.atlassian.com/fr/git/tutorials/git-bash), Git Bash est une application pour les environnements Microsoft Windows qui fournit une couche d'émulation pour une expérience de ligne de commande Git. Concrètement, Git Bash est un package qui installe Bash, quelques utilitaires Bash courants et Git sur un système d'exploitation Windows.

Ayant un ordinateur portable qui tourne sous Windows, la première étape pour moi est donc d'installer Git Bash, ce qui me permettra comme on le verra plus tard de pouvoir modifier ma page web. Je me suis alors rendu sur [ce site](https://git-scm.com/downloads) pour l'installer. 

Un problème que j'ai rencontré pendant cette étape est que j'avais suivi le tutoriel FabZero dont la première étape consistait à seulement télécharger Bash. C'est ce que j'ai fait, mais pour une raison qui m'échappe je n'ai jamais réussi à lancer l'application. Après avoir passé une bonne heure sur stack overflow sans solution, j'ai demandé de l'aide à [Quentin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/quentin.stevens/) qui est étudiant en informatique qui m'a alors parlé de Git Bash. Merci Quentin !


### Terminal de commande

Après avoir installé Git Bash, j'ai passé un peu de temps à me familiariser avec un terminal de commande. Avec notamment les commandes:

| Commande | Description
| :-----  | :----- 
| `cd` | Permet de se déplacer dans les répertoires.
| `ls` | Répertorie les fichiers et répertoires contenus dans le répertoire actuel.
| `mkdir [dossier]` | Crée un nouveau dossier avec le nom spécifié.
| `rm [fichier]` | supprime le fichier spécifié.


## Clés SSH

Pour moi, la création et la connexion des clés SSH a été la partie la plus complexe de ce module. Je ne suis toujours pas sûr d'avoir compris tout ce que j'ai fait et le tout m'a paru *mystique* voir *kafkaesque*. 

Selon Wikipédia, les clés SSH sont des clés de chiffrement (c'est-à-dire, des chaines de caractères, des codes uniques qui permettent dans le cadre de la sécurité des données de crypter et décrypter des données) qui permettent d'établir une connexion sécurisée entre mon pc et le serveur hébergeant mon site.
Les clés SSH sont spécifiques au [Secure Shell Protocol (SSH)](https://en.wikipedia.org/wiki/Secure_Shell), elles ont la particularité qu'on en utilise deux différentes:

  - Une **clé publique** utilisée pour le chiffrement.

  - Une **clé privée** utilisée pour le déchiffrement, qui est gardée secrète et qui ne peut pas se déduire de la clé publique.

Pour réaliser toutes les étapes qui suivent, j'ai majoritairement utilisé [ce guide](https://docs.gitlab.com/ee/user/ssh.html).

  1. La première étape consiste à générer les clés. Pour cela j'ai choisi l'algorithme **ED25519** car c'était celui qui était recommandé par le guide. L'image suivante montre comment j'ai généré mes clés.
  ![](images/module1/KeyGenCompressed.jpg)

  2. Après avoir généré mes clés, j'ai dû les lier à mon compte Gitlab. Pour cela, je me suis connecté sur Gitlab et j'ai été dans **User Settings → SSH Keys** et j'y ai rajouté ma clé publique.

  3. L'étape suivante consiste à cloner le répertoire Gitlab depuis le serveur sur mon pc, pour cela, je tiens à faire une dédicace à [Cosmin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cosmin.tudor/) pour ces explications, j'aurais pu passer encore beaucoup de temps à chercher comment faire le clonage sans lui. 
  Pour réaliser le clonage SSH j'ai exécuté la ligne de code suivante:
  'git clone git@gitlab.com:fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn.git'
  ![](images/module1/gitclone.png)

Une fois le répertoire sur mon pc, j'ai pu enfin passer aux choses sérieuses et réaliser ma première modification avec Git.

## Git

Git est un outil qui permet de suivre les modifications apportées à un ensemble de fichiers au fil du temps, en conservant les versions précédentes. Les commandes Git les plus courantes sont:


| Commande | Description
| :-----  | :----- 
| `git status` | Permet de vérifier l'état actuel du répertoire local par rapport à celui sur le serveur. Elle affiche les fichiers modifiés, les fichiers ajoutés ou supprimés.
| `git commit -m "message de validation"` | Permet de sauvegarder les modifications sur le répertoire local. Elle crée un instantané des modifications et les enregistre dans l'historique de version de Git.
| `git push` | Permet d'envoyer les modifications sur le répertoire local vers le serveur. Cela permet aux autres membres de l'équipe de récupérer vos modifications et de les intégrer dans leur propre version du code.
| `git pull` | Permet de récupérer les dernières modifications du serveur et de les fusionner avec la version présente sur mon pc. Cela permet de s'assurer que je travaille toujours sur la version la plus récente du code.

En utilisant ces commandes, j'ai pu mettre ma page personnelle à jour et écrire cette documentation pour le dernier module. Merci à Denis de m'avoir montré comment utiliser toutes ces commandes jusqu'à aboutir à ma première modification de ma page, c'était gratifiant. 


La dernière pièce manquante pour la modification du site est l'utilisation de Markdown pour la mise en page du site.


## Markdown

Selon [Wikipédia](https://fr.wikipedia.org/wiki/Markdown), Markdown est un langage de balisage léger qui a pour but d'offrir une syntaxe facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières. Pour apprendre comment l'utiliser, j'ai notamment utilisé [ce site](https://www.markdownguide.org/basic-syntax/) pour apprendre la syntaxe et [celui-là](https://docs.gitlab.com/ee/user/markdown.html) pour des fonctionnalités plus avancées.

![markdownIllustration](images/module1/MarkdownCompressed.jpg)

Voici un bref aperçu de différents éléments de syntaxe de Markdown:

| Commande | Résultat
| :-----  | :----- 
| `# Titre` | #Titre
| `## Sous-titre` | ## Sous-titre
| `**Gras**` | **Gras** 
| `*Italique*` | *Italique* 
| `***Gras et Italique***` | ***Gras et Italique*** 
| `![titre de l'image](images/TestScreenCompressed.jpg)` | ![Image](images/module1/TestScreenCompressed.jpg)


Un tuyau très pratique que m'a donné Denis est qu'avec la commande `mkdocs serve` on peut faire tourner un serveur sur son propre pc. Ce qui permet d'avoir un aperçu des modifications effectuées sans devoir attendre que le serveur se mette à jour.


## Compression d'image

Un autre aspect qui a été demandé de prendre en considération ici est de diminuer la taille des images en les compressant. Ce qui permet un affichage rapide de la page et limite la taille du stockage nécessaire.

D'après [Adobe](https://www.adobe.com/be_fr/creativecloud/file-types/image/comparison/jpeg-vs-png.html) Le format JPEG est conçu pour stocker efficacement des photos numériques de qualité, riches en détails et en couleurs. Il compresse les images volumineuses en fichiers de petite taille pour en faciliter le partage et le chargement en ligne. Mais cela coûte une perte de détails. A contrario, le format PNG utilise une méthode de compression sans perte. Aucune donnée n'est perdue quand un fichier d’image est compressé et la qualité reste intacte quel que soient le nombre de modifications et d’enregistrements.

Dans le cadre de cours, une légère perte de qualité des images est tolérable, je vais donc principalement utiliser du JPEG. J'utilise régulièrement Photoshop, je sais donc comment l'utiliser pour modifier la taille de mon image ou bien la compresser en JPEG. 

Pour illustrer la compression d'image, voici une image que j'ai compressée d'une taille initiale de 404Ko à 67.1Ko

![](images/module1/Wave.jpg)

![](images/module1/WaveCompressed.jpg)

Pour différencier les images compressées des images non compressées, je rajouterais le terme "Compressed.jpg" dans le nom du fichier, je ferais cette compression systématiquement pour les fichiers trop lourds qui dépassent 300Ko par exemple.

## CSS

Ayant un peu d'expérience en design de site web, j'ai rajouté un fichier [CSS](https://developer.mozilla.org/fr/docs/Web/CSS) à ma configuration markdown pour pouvoir personnaliser ma documentation. CSS est un langage de programmation qui est utilisé pour décrire la présentation visuelle d'un document écrit en HTML. Autrement dit, c'est un fichier qu'un navigateur web va lire pour savoir comment afficher le contenu d'un site. En utilisant CSS, je peux modifier l'apparence de mon document Markdown, comme la couleur de fond, la police de caractères, la taille des titres, etc. Ce fichier est accessible sous le nom *extra.css*.

## Management de projet

Je n'ai jamais vraiment établi de méthode de travail pour un cours, mais afin de gérer efficacement mon temps, je vais en élaborer une pour celui-ci. Mon plan d'action est le suivant :

Chaque semaine, j'ai au moins 4 heures de temps libre, soit le jeudi matin, soit le vendredi matin, et je prévois d'allouer ce temps spécifiquement pour ce cours. Pendant le cours, Denis a présenté certaines méthodes de travail que j'aimerais appliquer ici, comme travailler en parallèle sur ma documentation et les différents travaux assignés. Je prévois d'appliquer la méthode de la spirale, qui consiste à commencer par faire une version rapide d'une tâche avant de passer à une autre, puis de revenir à ce qui a été fait pour l'améliorer en fonction du temps restant.

Bien entendu, je prévois d'ajuster ma méthode de travail en fonction de mon évolution dans le cours et de mon rythme de travail.

## Bilan de fin de module

Concluons ce module en vérifiant que la [checklist de tâches](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/1) a bien été suivie 

1. Faire un site web et décrire comment il a été fait. ✔️
2. Présente-toi. ✔️
3. Documente les étapes suivies pour télécharger les fichiers depuis le serveur GitLab. ✔️
4. Documente les étapes suivies pour compresser les images pour qu'elles occupent un petit espace de stockage. ✔️
5. Documente comment tu comptes utiliser les principes de management de projet. ✔️
6. Push ce que tu as fait sur le serveur. ✔️

