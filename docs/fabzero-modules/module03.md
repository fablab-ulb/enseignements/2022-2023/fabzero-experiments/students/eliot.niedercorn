# 3. Impression 3D

Le troisième module du cours FabZero a pour objectif d'apprendre à utiliser des logiciels de slicing et des imprimantes 3D pour pouvoir imprimer la pièce modéliser au [module 2](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn/fabzero-modules/module02/).

Comme j'utilise souvent mon imprimante 3D personnelle pour mes études ou pour mes projets, je suis déjà familier avec les notions présentées dans ce module.


## Slicer

Un [slicer](https://fr.wikipedia.org/wiki/Logiciel_de_d%C3%A9coupage_en_tranches) est un logiciel qui permet de convertir des objets modélisés par ordinateurs avec des logiciels de CAD en des fichiers d'instructions que les imprimantes 3D peuvent lire et utiliser pour imprimer l'objet. Plus précisément, ces logiciels permettent de convertir des fichiers STL (ou équivalents) en fichiers g-code contenant des paramètres d'impression spécifiques tels que la précision de l'impression ou la nécessité d'utiliser un support.

Le slicer utilisé dans ce module est [PrusaSlicer](https://help.prusa3d.com/article/install-prusaslicer_1903) qui est un logiciel open-source et gratuit. Il est notamment compatible avec les imprimantes Prusa qui sont celles utilisées au FabLab mais également avec d'autres types d'imprimantes.

Voici à quoi ressemble ma pièce une fois ouverte avec PrusaSlicer:

![](images/module3/CatapultPrusaCompressed.jpg)

Le panneau sur la droite permet de sélectionner certains préréglages de qualités ainsi que la quantité de remplissage de la pièce et la présence d'une bordure ou non. D'autres paramètres sont accessibles dans l'onglet *Réglages d'Impression*. J'explique les paramètres principaux dans le tableau suivant:


| Paramètre | Description
| :-----  | :----- 
| *Remplissage* | Densité de remplissage de la pièce, plus la pièce est dense et au plus, elle sera solide.
| *Hauteur de couche* | Hauteur de chaque couche imprimée, plus elle est petite, plus la pièce est précise.
| *Température d'extrusion* | Température à laquelle le filament est fondu pour l'impression (dépend de la nature du filament utilisé).
| *Vitesse d'impression* | Vitesse à laquelle la buse d'impression se déplace pendant l'impression, plus elle est rapide moins l'impression est précise.
| *Type de support* | Type de structure utilisé pour soutenir les parties surplombantes de la pièce imprimée.

Avant de lancer l'impression, il faut savoir qu'un des causes pour lesquelles une impression 3D peut échouer est que la pièce se détache du plateau pendant l'impression. C'est pourquoi il est important d'assurer une bonne adhésion de la pièce au plateau, pour cela, on nettoie le plateau avant de lancer l'impression.


## Impression de la pièce

Après avoir chipoté à tous ses paramètres, j'ai pu générer mon g-code (Accessible en tant que *FlexCatapulte_0.2mm.gcode* sur mon gitlab) et lancer l'impression de ma pièce. Les imprimantes du FabLab sont des Prusa MK3 dont voici une photo:

![](images/module3/PrusaMK3Compressed.jpg)

J'ai choisi pour cette première impression des paramètres de résolution assez grossiers en me disant que je n'allais surement pas être satisfait du résultat dès le premier coup, mais au final, je suis assez content du résultat:

![](images/module3/Result.jpg)

On peut constater ici que ma pièce est bien flexible.

![](images/module3/Compliance.jpg)



## Problème de dimensionnement

Après avoir imprimé ma pièce, j'ai remarqué qu'elle ne s'emboîtait pas correctement avec les Lego. Cela est dû au fait que j'ai conçu la pièce avec des dimensions précises correspondant exactement à celles d'un Lego, à savoir un rayon de 2,4 mm, sans prendre en compte les tolérances d'imprécision de la machine. J'aurais dû laisser un peu de jeu dans le dimensionnement pour tenir compte de ces écarts possibles. En parlant de ce problème à Denis, il m'a conseillé d'imprimer une pièce test qui contient plusieurs trous de rayons différents, ce qui permettrait de voir quelle serait le rayon idéal correspondant à un Lego.

C'est ce que j'ai donc fait. Voici la pièce (Accessible en tant que *HolesVerification.scad* sur mon GitLab): 

![](images/module3/HolesVerification.jpg)

Je l'ai imprimé avec des paramètres de résolutions et de remplissage très bas pour que ça aille vite et pour économiser du matériel étant donné qu'il s'agit seulement d'un test. Malheureusement, j'ai fait une erreur de programmation lors de l'impression en indiquant de mauvais rayons, ce qui fait que j'ai dû recommencer l'impression. Voici le résultat, la pièce du bas est celle pour laquelle je me suis trompé et celle du haut est la bonne:

![](images/module3/PrintedHolesVerification.jpg)

L'imprimer m'a permis de me rendre compte que le rayon idéal pour les trous est de 2.45 mm, j'ai pu adapter le code de ma pièce en conséquence.


## Kit

Avant de se lancer dans la construction de notre kit, dans une démarche d'entraide, Martin et moi avons réalisé des tests de résistances sur nos pièces respectives, ce qui m'a permis de mettre en évidence la fragilité de sa pièce:

![](images/module3/MartinFailureCompressed.jpg)

Après ce test, nous avons décidé avec [Martin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/), [Emma](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/emma.dubois/) et [Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/) de réaliser nos kits ensemble. Martin a conçu un ressort flexible, Emma une pièce flexlink tandis que Suzanne a conçu une pièce Lego sur laquelle notre kit prend support. Trouver un moyen de combiner toutes ces pièces ensemble était un peu compliqué, mais au final, nous avons abouti au design suivant:

![](images/module3/Combo.jpg)

Je trouve notre kit assez chouette, car nous sommes partis de composants qui n'avaient rien à voir à priori pour former cette structure qui profite de nos pièces! On y a placé un siège pour un petit personnage Lego qu'on peut catapulter. On peut voir notre kit en action [ici](https://www.youtube.com/shorts/2lgtvRBjNhY)


## Bilan de fin de module

Concluons ce module en vérifiant que la [checklist de tâches](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/3) a bien été suivie 

1. Explique ce que tu as appris en utilisant les imprimantes 3D. ✔️
2. Montre comment tu as réalisé ton kit avec des mots/screenshots. ✔️
3. Inclus les fichiers utilisés pour l'impression 3D (CAD et STL) ✔️
4. Inclus des images et une description de ta pièce. ✔️
5. (Tâche du module précédent) Documente comment tu as utilisé le travail d'autres personnes pour compléter ton kit. ✔️