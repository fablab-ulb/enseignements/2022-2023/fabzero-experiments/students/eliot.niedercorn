# 2. Conception Assistée par Ordinateur (CAD)

Le deuxième module du cours FabZero a pour objectif d'apprendre à utiliser des logiciels de design CAD (Computer Assisted Design) et d'apprendre comment fonctionne les licences de droits d'auteur Creative Commons.

Je suis déjà habitué à certains logiciels de CAD, mais par contre, je ne connais rien en matière de droits d'auteurs. Commençons par les logiciels de CAD.

## Logiciels de CAD
Les logiciels de CAD sont des programmes qui permettent de créer, modifier, analyser et simuler des dessins techniques en 2D ou en 3D. Ils sont utilisés dans plusieurs domaines comme l'architecture, l'ingénierie, la conception de produits, etc. Nous allons notamment utiliser ce genre de logiciel pour concevoir des pièces en 3D dans le but de les imprimer.

Je suis déjà habitué aux logiciels de CAD, car j'ai fait beaucoup de projets les utilisant dans mes études et en dehors.
J'utilise principalement [SolidWorks](https://www.solidworks.com/fr) mais pour ce cours, on nous a présentés différents logiciels de CAD qui ont l'avantage d'être gratuits et Open-source. 
Ça tombe bien, comme ma licence étudiant SolidWorks a expiré.

Ces différents logiciels présentés durant le cours sont:

- [**InkScape**](https://inkscape.org/fr/)
- [**OpenSCAD**](https://openscad.org/)
- [**FreeCAD**](https://www.freecad.org/)

Ces logiciels, même s'ils permettent de conçevoir des objets similaires, ont des fonctionnalités et des approches différentes:

**Inkscape** est un logiciel de dessin vectoriel qui permet de créer des dessins en 2D. Il sera plutôt utilisé pour réaliser des logos, des illustrations, etc.

**OpenSCAD** est un logiciel qui permet de créer des modèles 3D en utilisant des lignes de codes, il a l'avantage de permettre de conçevoir ses dessins techniques de manière paramétrique, ce qui permet de les modifier très rapidement. Voici pour illustrer la première pièce que j'ai réalisée en OpenSCAD pendant le cours:

![](images/module2/OpenSCAD.jpg)

**FreeCAD** est un logiciel qui permet aussi de créer des modèles 3D, mais qui utilise plus un système de contraintes pour créer son dessin qu'un système paramétrique. Notons qu'il a l'air d'offrir le plus de fonctionnalités parmi les 3. De ce que j'ai pu comprendre, ce logiciel se rapproche le plus de SolidWorks.


## FlexLinks
La première étape du module consiste à faire le design et la modélisation d'un mécanisme [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks) dans le but de pouvoir l'imprimer en 3D lors du prochain module. FlexLinks désigne des [mécanismes compliant](https://fr.wikipedia.org/wiki/M%C3%A9canisme_flexible) qui sont désignés pour fonctionner avec des pièces LEGO. Ce sont pièces constituées de liaisons à articulations élastiques et de pièces LEGO permettant de conceptionner rapidement des dispositifs profitant des propriétés des mécanismes compliant. En voici un exemple avec la pièce satellite:

![](images/module2/SatelliteCompressed.jpg)


J'ai décidé de réaliser une pièce similaire à la pièce satellite, car son design m'a directement fait penser à une catapulte, ce qui est pour moi la fonctionnalité la plus intéressante que je puisse réaliser parmi les pièces FlexLinks. Autrement dit, je trouve ça rigolo de tirer des projectiles sur des gens.

En plus, réaliser cette pièce est un challenge par rapport à d'autres, car [le lien thingiverse de la pièce n'existe plus](https://www.thingiverse.com/thing:3016935), je n'ai donc pas accès à un preview de la pièce. 


Pour faire cette pièce, j'ai choisi d'utiliser OpenSCAD par rapport à FreeSCAD pour plusieurs raisons:

- Il a l'air plus simple et rapide à prendre en main.
- J'aime bien coder et la fonctionnalité de paramétrisation du dessin qu'offre OpenSCAD.
- Je suis habitué au logiciel de type SolidWorks comme FreeSCAD et je veux tester un autre type de logiciel.

Pour commencer la conception de la pièce, je me lance sans choisir de dimensions précises. Je regarderai plus tard comment je dois adapter mes dimensions aux pièces LEGO, c'est l'avantage de la modélisation paramétrique. J'utilise aussi cette [cheatsheet](https://openscad.org/cheatsheet/) pour voir toutes les commandes auxquelles j'ai accès.

J'ai désigné ma catapulte séquentiellement en commençant par la tête. Pour la faire, j'ai commencé par un rectangle que j'ai arrondi avec la commande `minkowski()`
![](images/module2/1.png)

J'ai ensuite fait des trous dans la tête en utilisant la commande `difference()` avec des cylindres, j'ai aussi appris à utiliser la commande `module()` qui est une sorte de fonction: 

![](images/module2/2.png)

L'étape suivante consiste à faire la barre flexible de la catapulte:

![](images/module2/3.png)

La quatrième étape est de faire la base de la catapulte. Pour cela, j'ai rajouté un rectangle:

![](images/module2/4.jpg)

La cinquième étape est la plus compliquée, car il faut rajouter un rectangle avec un angle dans lequel est foré des trous. Tout ça en fonction des tailles des pièces précédentes pour que la paramétrisation fonctionne.

![](images/module2/5.jpg)

La dernière étape consiste à retirer le matériel excédentaire pour ne perdre de temps lors de l'impression et économiser du matériel d'impression.

![](images/module2/6.jpg)

**Et voilà !** Je suis plutôt content du design final, je me suis écarté de la pièce satellite en ayant pour objectif de faire une pièce qui ressemble le plus à une catapulte. Comme on le verra plus loin dans la documentation, j'ai rencontré un problème avec ce premier design. C'est pourquoi ce premier design est accessible sur mon GitLab sous le nom *OldFlexCatapult.scad* tandis que le design final est sous le nom *FlexCatapult.scad*.


## Dimensionnement Final

Il me reste maintenant pour finir la pièce à l'adapter aux dimensions des pièces LEGO, j'ai lu sur un forum que OpenSCAD n'a pas besoin de travailler avec une unité de distance spécifique. À la place, lorsqu'on va importer notre fichier dans un logiciel tiers, celui-ci va directement nous demander comment interpréter les unités. Par exemple comme *cm* ou *mm*. Il faut donc juste s'assurer que les ratios de distance des différents éléments de ma pièce soient corrects.

Pour cela, je me suis rendu sur [Wikipedia](https://fr.m.wikipedia.org/wiki/Lego) où j'ai trouvé cette image:

![](images/module2/LegoDimensions.svg)

Il a donc fallu que je choisisse un diamètre de trou de 4.8 mm et un espace entre les trous de 3.2mm. Lorsque j'ai mis ces dimensions dans mon code, le problème est apparue:

![](images/module2/ProbDimLego.jpg)

Je me suis rendu compte que je n'avais pas assez bien paramétrisé mon code... J'ai donc dû me replonger dedans pour faire une bonne paramétrisation. Ce qui m'a forcé si je ne voulais pas passer trop de temps dessus à changer légèrement le design. Voici le résultat final avec les dimensions correctes:

![](images/module2/FinalDesign.jpg)

Comme dit précédemment, il est accessible sur mon GitLab sous le nom *FlexCatapult.scad*


## Creative Commons Licenses

Un autre sujet important abordé durant ce module est l'utilisation de licence de droits d'auteur. Pour cela, nous considérons les [licences Creative Commons](https://creativecommons.org/about/cclicenses/) qui sont un ensemble de licences de droits d'auteur gratuites et open sources qui permettent aux créateurs de partager leur travail avec le public tout en conservant certains droits, tels que la reconnaissance de la paternité et le contrôle de l'utilisation commerciale de leur création. Ces licences se séparent en 6 catégories différentes, chacune offrant différents niveaux de protection de droits d'auteur. En voici une description:

| Licence | Description
| :-----  | :----- 
| Licence **CC BY** | Cette licence autorise à distribuer, modifier et utiliser l'œuvre à des fins commerciales ou non, à condition de créditer l'auteur original.
| Licence **CC BY-SA** | Cette licence autorise à distribuer, modifier et utiliser l'œuvre à des fins commerciales ou non, à condition de créditer l'auteur original **et** de distribuer l'œuvre dérivée sous la même licence que celle de l'œuvre originale.
| Licence **CC BY-NC** | Cette licence autorise à distribuer, modifier et utiliser l'œuvre à des fins **non** commerciales et à condition de créditer l'auteur original.
| Licence **CC BY-NC-SA** | Cette licence autorise à distribuer, modifier et utiliser l'œuvre à des fins **non** commerciales et à condition de créditer l'auteur original **et** de distribuer l'œuvre dérivée sous la même licence que celle de l'œuvre originale.
| Licence **CC BY-ND** | Cette licence autorise à distribuer et utiliser l'œuvre à des fins commerciales ou non **et** n'autorise pas à dériver ou adapter l'œuvre originale.
| Licence **CC BY-NC-ND** | Cette licence autorise à distribuer et utiliser l'œuvre à des fins **non** commerciales **et** n'autorise pas à dériver ou adapter l'œuvre originale.

Il existe aussi une dernière "licence" la **CC0** qui consiste à donner son œuvre au domaine public. Dans ce cas, n'importe quelle personne peut distribuer, modifier et utiliser l'œuvre original sans conditions.

Pour appliquer une licence Creative Commons, c'est assez simple, il suffit de communiquer clairement que le travail est protégé par une licence et ces conditions. J'ai par exemple décidé de rajouter une licence **CC BY-SA** pour ma catapulte en rajoutant la ligne de code suivante:

```
/*
* "FlexCatapult"
* Copyright (c) [2023], Eliot Niedercorn
*
* Ce projet est sous licence Creative Commons Attribution-ShareAlike [Attribution-ShareAlike 4.0 International]
* Pour voir une copie de cette licence, visitez https://creativecommons.org/licenses/by-sa/4.0/legalcode
*/
```


## Bilan de fin de module
Concluons ce module en vérifiant que la [checklist de tâches](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/2) a bien été suivie 

1. Modéliser un système FlexLinks en utilisant de la paramétrisation. ✔️
2. Expliquer comment la modélisation a été faite en utilisant des screenshots. ✔️
3. Inclure les fichiers designés. ✔️
4. Inclure une licence CC au travail. ✔️
5. Documenter comment tu as utilisé le travail d'autres personnes pour compléter ton kit.  →  Par soucis de cohérence, j'en parle dans le prochain module.



