# 5. Arbre de problème et Formation des groupes

Pour ce cinquième et avant-dernier module, nous avons:

- **Premièrement**, par groupe de 2 comme objectif de réaliser un [arbre de problèmes](https://www.youtube.com/watch?app=desktop&v=9KIlK61RInY) et l'arbre des solutions correspondant.
J'ai décidé de faire équipe avec [Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/) pour cette tâche dans laquelle nous avons décidé d'aborder la thématique de la low-tech.

- **Deuxièmement**, lors du cours correspondant, dû ramener un objet qui représente un thème ou une problématique qui nous tient à cœurs. Ces objets ont été utilisés pour former les groupes pour le projet final et ont été une base pour le brainstorming qui a suivi.

- **Troisièmement**, lors du cours correspondant, appris des outils de dynamique de groupe et de gestion de projet.


## Les low-tech

Les [low-tech](https://fr.wikipedia.org/wiki/Low-tech) représentent un mouvement qui prône un retour à des solutions simples, durables et écologiques pour répondre aux besoins humains. Elles sont typiquement peu coûteuses, accessibles à tous et répondent à des besoins de base tels que se nourrir, se chauffer, se déplacer et se loger. Elles promouvoient un mode de vie plus sobre, respectueux de l'environnement et accessible à tous.


Pour nous donner de l'inspiration par rapport à l'arbre de problèmes, Denis nous a présenté au cours précédent plusieurs projets de FabLab et de sciences frugales et nous a notamment parlé du [low-tech Lab](https://lowtechlab.org/fr) et de l'expérience de son co-fondateur, Corentin de Chatelperron, qui est resté 4 mois à bord d’une plateforme flottante ancrée au large de la Thaïlande en totale autarcie avec 30 technologies low-tech qui lui ont permis de répondre à ses besoins vitaux. Un [documentaire ARTE](https://www.youtube.com/watch?v=fNRjRXFecT4) qui le suit pendant son expérience a été fait et Suzanne qui l'avait déjà vu m'a conseillé de le regarder. J'ai trouvé ce documentaire vraiment intéressant et ça m'a poussé à choisir cette thématique. En plus de cela, j'ai lu [*L'Âge des low tech*](https://www.seuil.com/ouvrage/l-age-des-low-tech-philippe-bihouix/9782021160727) de Philippe Bihouix qui défend l'idée que nous devons nous tourner vers les low-tech pour répondre aux enjeux environnementaux, économiques et sociaux actuels, j'ai également trouvé ce livre très intéressant ce qui me pousse d'autant plus dans cette thématique.

### L'arbre de problèmes 

L'arbre de problèmes peut se construire en suivant les étapes suivantes:

  - Identifier un problème principal (le tronc de l'arbre).
  - Pour ce problème, identifier les causes primaires (les racines de l'arbre) et les causes derrières ces causes primaires (les racines profondes).
  - Toujours pour ce problème, identifier les conséquences directes (les branches de l'arbre) et les effets secondaires (leurs ramifications). 

Les low-tech peuvent répondre à de nombreux problèmes comme la pollution, le changement climatique, l'extraction massive des ressources rares, les inégalités sociales, les dépendances technologiques, la perte des savoir-faire et l'obsolescence programmé. Pour réaliser l'arbre de problème, nous allons englober tous ces problèmes comme des conséquences de la structure actuelle de la société, ce qui est un peu grossier, car ces problèmes ont des origines multiples et complexes, et on fait cette simplification seulement dans le cadre de cet exercice.

On a donc notre **tronc** : *la structure actuelle de la société*. Il reste maintenant à regarder quelles sont les racines et les branches correspondantes.

#### Les racines

Certaines causes de la structure actuelle de la société sont:

  - La **surconsommation** qui a pour racine profonde la volonté de croissance économique, le manque de sensibilisation, l'appât du gain et la recherche du profit à court terme.

  - L'**individualisme** qui a pour racine profonde les politiques publiques et l'enfermement sociale avec les nouvelles technologies, les médias sociaux et l'urbanisation. (Même s'il faut noter que l'individualisme est peut-être pas le meilleur mot pour exprimer ce que je voulais dire, quelqu'un peut voir l'individualisme comme quelque chose de nécessaire à la créativité et l'innovation.)

  - Le **manque d'éducation et de sensibilisation** lui aussi qui a pour racine profonde les politiques publiques.


#### Les branches

Comme on en a déjà parlé, certaines conséquences de la structure actuelle de la société sont:

  - Le **réchauffement climatique** et la pollution qui a pour ramification entre autres: l'augmentation du niveau des océans, la diminution de la biodiversité, l'augmentation des sécheresses, des inondations et des pénuries alimentaires.

  - L'**extraction massive** des ressources rares qui a pour ramification le réchauffement climatique et la pollution ainsi que l'exploitation de communautés locales et des instabilités géopolitiques.

  - La **dépendance technologique** qui pour ramification la perte des savoir-faire.


Voici notre arbre de problème qui reprend certains des points discutés plus hauts:

![](images/module5/ArbreDeProblemeCompressed.jpg)

### L'arbre des solutions

L'arbre des solutions se construit de manière similaire à l'arbre de problèmes:

  - Reformuler le problème en tant qu'un objectif (tronc).
  - Reformuler les causes comme des activités à développer pour répondre au problème (racines).
  - Reformuler les conséquences du problème comme les conséquences des activités qui le résout (branches). 

En opposition avec l'arbre de problèmes, notre **tronc** ici est *un modèle sociétal équitable, inclusif et éco-responsable*.

![](images/module5/ArbreSolutionsCompressed.jpg)


#### Les racines
Certaines activités à développer sont:

 - La **mise en place de campagnes de sensibilisations** pour sensibiliser les individus aux enjeux sociaux et environnementaux et encourager les initiatives citoyennes.

 - La **prise d'actions politique concrète** qui incitent la coopération et la solidarité et qui changent la structure actuelle.


#### Les branches
Les conséquences de certaines de ces activités sont

 - Le **ralentissement du changement climatique** par l'utilisation de technologies plus durables et respectueuses de l'environnement.

 - Une **société plus égalitaire** par l'encouragement des collaborations entre les individus et les communautés.



## La formation des groupes

Comme dit précédemment, la deuxième partie de ce module consiste en la formation des groupes autour de nos objets et du brainstorming qui en a suivi.

### Objet ramené

J'ai choisi de ramener une pierre volcanique que j'ai ramassée lors de mon voyage sur le mont Etna pour illustrer la problématique de l'extraction des ressources. Je voulais discuter de la manière dont l'extraction de ces ressources affecte notre environnement et notre société.
Lors du cours, nous avons dû nous mettre en cercle au centre duquel était déposés tous nos objets, l'étape suivante consistait à circuler entre les objets et à discuter de nos problématiques. Nous nous sommes ensuite mis en groupe en fonction de la similarité de nos objets/problématiques.


### Formation du groupe

Après avoir discuté avec les différents élèves de leurs projets, nous avons constitué une équipe de 4 chacun ayant pris des objets liés à la nature:

  - [Antoine](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/antoine.trillet/) a ramené une branche morte, desséché par rapport à la problématique de la sécheresse et de la régularisation de l'eau.

  - [Halil](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/halil-ibrahim.cela/) a ramené un pot de jardinage avec une plante morte à l'intérieur lui aussi par rapport à la problématique de la sécheresse.

  - [Martin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/) a ramené une géode pour lui aussi illustrer la problématique de l'extraction des ressources.

Nous avons donc tous les 4 des problématiques similaires en tête! On a pris une photo pour immortaliser la création de notre groupe: 

![](images/module5/GroupeCompressed.jpg)

### Brainstorming

Après la formation de notre groupe, nous avons brainstormé pour essayer de trouver un maximum de liens entre nos objets/problématiques.
Voici la feuille sur laquelle nous avons noté tous les liens auxquelles nous avons pensé:

![](images/module5/LiensObjetsCompressed.jpg)

Après avoir établi tous ces liens, nous avons dû réfléchir à des thématiques qui ressortaient de notre groupe et de nos problématiques.
Après discussion, nous avons établi thématiques intéressantes: l'eau, les ressources, l'escalade (car on s'est rendu compte qu'on aimait tous l'escalade)
et la low-tech/récup.

Nous avons dû trier ces thématiques en fonction de notre enthousiasme individuelle pour travailler dessus. Voici le résultat: 

![](images/module5/1.jpg)

Faire cet exercice nous as permis de se rendre compte qu'on pouvait clairement écarter la thématiques des ressources pour laquelle le groupe était le moins enthousiaste et qu'on pouvait se concentrer les thématiques de l'escalade et de l'eau qui sont clairement celles pour lesquelles on est le plus motivés.

L'exercice suivant consistait à commencer à réfléchir à des problématiques liées à nos thématiques préférées. Après discussions, nous sommes aboutis à ce résultat:

![](images/module5/2.jpg)


### Discussion avec les autres groupes

Après avoir établi certaines problématiques, nous avons eu l'occasion d'échanger avec d'autres groupes, qui ont pu nous faire réfléchir à d'autres problématiques auxquelles on n'avait pas forcément pensé comme la désalinisation de l'eau de mer. Voici les nouvelles problématiques qui sont ressorties des discussions:

![](images/module5/3.jpg)

## Fonctionnement du groupe
Comme dit précédemment, la troisième partie de ce module consiste à documenter ce que j'ai appris lors du cours sur les outils de dynamique de groupe et de gestion de projet.

Beaucoup de points ont été abordés durant ce cours que je résume ici:

### Outils de communication

Dans un groupe, il est essentiel de permettre une communication efficace et équitable. Pour cela, on nous a présenté plusieurs outils de communication:

  - Le bâton de parole : il permet à chaque membre de prendre la parole pendant une période de temps raisonnable et équitable, en évitant les interruptions et les monopoles de parole.
  - La file d'attente : les membres peuvent signaler leur intention de parler en montrant un chiffre avec leur main, indiquant leur place dans la file d'attente.
  - La température : les membres utilisent des gestes pour exprimer leur niveau d'accord ou de désaccord avec les propos de l'orateur. Par exemple, on pourrait lever le pouce vers le haut pour indiquer l'accord, et vers le bas pour le désaccord. Cela permet d'avoir une vue d'ensemble rapide des avis du groupe sans que chacun doive prendre la parole.

Un autre point est qu'il est recommandé de parler en «je» pour ne pas faire de généralisation.

### Structure de réunion

Pour faire une bonne réunion, il est essentiel qu'elle soit bien structurée. Pour cela, une bonne pratique est de préparer un ordre du jour avant la réunion et de donner certains rôles au membre de la réunion. Par exemple :

  - Le gestionnaire du temps : il veille à ce que la réunion reste dans les limites de temps prévues pour chaque point de l'ordre du jour.
  - Le secrétaire : il prend des notes précises sur les décisions prises et les actions à suivre, afin d'assurer une trace commune des réunions pour une meilleure compréhension du suivi du projet.
  - L'animateur de réunion : il s'assure que tout le monde puisse donner son avis et que la réunion avance dans la bonne direction en encourageant une prise de parole constructive et en évitant les digressions.

Une autre bonne pratique est de prendre la météo d'entrée au début de la réunion pour que chacun sache connaisse l'attitude collective et l'état de chacun.


### Prise de décision

Plusieurs outils de prise de décision ont également été présentés. Chacune a des avantages et inconvénients, et il est important de choisir la bonne technique en fonction de la situation. Voici les principales :

  - Tableau avantage/désavantage : On liste tous les avantages et les désavantages d'une décision potentielle. Cela permet de peser les différents aspects et de prendre une décision éclairée.

  - Vote à la majorité : Comme son nom l'indique, on fait un vote à la majorité, c'est simple et rapide, mais ça ne prend pas en compte les opinions minoritaires.

  - Vote en fonction du domaine de compétence : On donne un poids différent aux votes en fonction de l'expertise de chaque personne.

  - Consentement : On prend la décision si personne n'y est opposé. Cela signifie que tout le monde doit être d'accord avec la décision, ou du moins ne pas s'y opposer. Ça a l'avantage d'être très rapide.

  - Vote classé : On classe les différentes options de vote par ordre de préférence. Cela permet de prendre en compte les opinions de chacun tout en évitant les désavantages du vote à la majorité.

### Application à notre groupe

Voici ce que nous avons décidé de mettre en place pour notre groupe suite à ce cours:

  - Nous allons structurer nos réunions en préparant pour chacune d'entre elle un ordre du jour et en prenant la météo d'entrée.
  - Nous avons décidé en utilisant la méthode du vote classé que Martin allait prendre le rôle du secrétaire, Antoine du gestionnaire du temps, Halil s'occupera de l'ordre du jour et moi de l'animateur et que nous échangerons ces rôles entre nous au fur et à mesure des réunions.
  - Étant donné que nous sommes quatre membres dans le groupe, nous avons décidé de ne pas utiliser d'outils de communication spécifiques. Nous avons constaté que chacun s'exprime de manière équitable sans monopoliser la parole, et si cela survient, l'animateur remettra le groupe sur la bonne direction.
  - Nous allons communiquer entre nous en utilisant le groupe Messenger que nous avons déjà créée lors de la formation du groupe.


## Bilan de fin de module

Concluons ce module en vérifiant que la [checklist de tâches](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/8) a bien été suivie 

### Par rapport à l'arbre de problème

1. Prendre inspiration des projets FabLab et des projets en sciences frugale présentés pendant le cours pour choisir une problématique dont tu te soucies. ✔️
2. Analyse la problématique et construit un arbre de problème et l'arbre des solutions. Poste les diagrammes et décrit les brièvement. ✔️


### Par rapport au cours du 07/03/23 - Formation des groupes

1. Décris ton expérience avec le processus que tu as suivi, de passer d'un objet à un choix de groupe, de thème et de brainstorming. ✔️
2. Décris l'objet que tu as choisi et liste les thématiques et problèmes possibles identifiés par le groupe. ✔️


### Par rapport au cours du 28/3/23 - Fonctionnement du groupe

Explique comment ton groupe fonctionne:

  1. Qu'est-ce que vous avez déjà mis en place ? ✔️
  2. Comment le groupe va structurer ses réunions ? ✔️
  3. Quels rôles avez vous identifiez pour les réunions et pour le projet en général. ✔️
  4. Comment allez-vous faire des décisions dans le groupe ? ✔️
  5. Comment allez-vous communiquer entre vous ? ✔️