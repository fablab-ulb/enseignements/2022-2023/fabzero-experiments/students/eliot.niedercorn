# 4. Découpe laser

Pour le quatrième module du cours FabZero, chaque étudiant pouvait choisir de suivre une formation sur une machine du Fablab. J'ai choisi la découpeuse laser, car c'est une machine avec laquelle je n'ai jamais travaillé et parce que j'aime les lasers, surtout que j'ai eu pleins de cours dessus.

Avant de me rendre à la formation, j'ai dû parcourir la [documentation sur les découpeuses lasers du Fablab](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut).


## Découpeuse Laser

Une découpeuse laser est une machine qui utilise un laser contrôlé en puissance pour découper ou graver dans différents matériaux. En chauffant la surface du matériau jusqu'à vaporisation. Comme le laser est très concentré en un point, il permet une découpe très précise.
La machine est contrôlée par ordinateur, on insère des fichiers 2D (par exemple du .svg) dans une application de découpe dédiée qui communique avec la machine.

En arrivant à la formation, nous avons dû répondre, à l'aide de la documentation, à [ce questionnaire](https://forms.office.com/e/kGfhUYSHTi) avant de pouvoir aller dans la salle des machines. J'ai réussi le quiz en deux essais.


## Formation

La formation portait sur le fonctionnement général d'une découpeuse laser, l'importance des paramètres de puissance et de vitesse, la sécurité et une présentation des 3 différentes découpeuses du Fablab.
Ces dernières ont chacune des caractéristiques et des fonctionnalités spécifiques utiles pour différentes applications, en voici un court résumé :

 - **L'Epilog Fusion Pro 32** qui est une découpeuse professionnelle avec son logiciel de découpe dédié et une caméra intégrée. 
![](images/module4/EpilogCompressed.jpg)


 - **La Full Spectrum Muse** est une découpeuse plus compacte qui convient aux particuliers qui ont moins d'espace. Elle possède aussi son logiciel de découpe dédié.
 ![](images/module4/MuseCompressed.jpg)

 - **La Lasersaur** a la particularité d'être une découpeuse open source qui a été construite au Fablab par les gens qui y travaillent, ces plans sont accessibles au grand public [ici](http://www.lasersaur.com/). Selon ce site, en plus que ses plans soient ouverts à tout le monde, elle propose un ratio performance/prix impressionnant. Je trouve ça assez chouette, car ça a dû prendre beaucoup d'efforts et le résultat est cool. Comme elle est open source, elle est flexible aux modifications et réparable relativement facilement.
![](images/module4/Lasersaur.jpg)

Au cours de la formation, nous avons appris à utiliser ces machines en toute sécurité. Je reprends ici les principales parties de la formation.

### Paramètres de puissance et de vitesse

Les deux paramètres essentiels lors d'une découpe laser sont la vitesse et la puissance. Par exemple, si la vitesse de la tête de la découpeuse est trop lente pour une puissance trop forte, alors le matériau risque de prendre feu. Choisir correctement ces paramètres permettent de contrôler si on réalise une découpe, une gravure marquée ou une gravure légère sur le matériau utilisé. Il faut faire attention au fait que pour chaque matériau, une découpe aura lieu pour différentes valeurs de ces paramètres. Nous avons également appris à importer nos fichiers dans les logiciels de contrôle des découpeuses.

Voici une image pour illustrer l'importance de ces deux paramètres. C'est une planche d'un matériau sur laquelle on a programmer la découpeuse laser pour passer sur chaque carré avec différentes valeurs de puissance et vitesse. On peut voir l'impact de ce choix sur le résultat final.

![](images/module4/TestLaser.jpg)

### Paramétrisation dans le logiciel

Un point important de la formation est pour découper un modèle de différentes manières, par exemple en gravant légèrement une partie et en découpant complètement une autre, alors, il faut dessiner ces différentes parties avec différentes couleurs dans les logiciels de découpe. Une fois notre modèle importé dans le logiciel, on peut sélectionner les paramètres de puissance et de vitesse pour chaque couleur différente.

### Sécurité

Les mesures de sécurité varient d'une machine à l'autre. En général, nous devons allumer l'extracteur de fumée, activer le système de refroidissement du laser et on ne peut pas regarder directement le faisceau laser à moins qu'on mette des lunettes de protection.

Il est important de rester à proximité de la machine jusqu’à la fin de la découpe pour pouvoir appuyer sur le bouton d'arrêt d'urgence en cas de problème. Il y a un bouton pause sur les machines si on doit s'absenter.

Il faut aussi ne jamais ouvrir la machine en cours d'utilisation et attendre un peu avant de l'ouvrir après la découpe pour que la fumée soit évacuée.

Après cette formation, nous avons enfin pu passer aux choses sérieuses.

## Partie pratique

Avant de venir à la formation, j'ai préparé le coup et conçu ma propre version du design "*Keep calm and*", j'ai conçu le design suivant sur Illustrator:

![](images/module4/OriginalDesign.svg)

[Antoine](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/antoine.trillet/) est tombé sous le charme du concept et on a fait équipe à deux. Voici une photo de nous deux avec nos lunettes de protection pendant la découpe:

![](images/module4/TeamLaser.jpg)

Chaque groupe a eu l'opportunité de travailler avec une machine différente, nous avons pu utiliser la lasersaur.

Suite à la formation, nous avons réalisé qu'il fallait modifier mon dessin, le découper correctement. On veut réaliser une gravure sur les lettres tandis qu'on veut découper l'objet en un rectangle depuis une plaque plus grande. Concrètement, on doit donc utiliser deux couleurs différentes pour pouvoir paramétriser chacune dans le logiciel. Cela a mené à ce design qui est le design final:

![](images/module4/ModifiedDesign.svg)

On a rencontré un problème lors de l'importation de notre nouveau fichier dans le logiciel de découpe. Les deux couleurs différentes que nous avions utilisées ne s'affichaient pas, et avions seulement du noir. Nous n'avons pas très bien compris la raison du problème, mais après en avoir discuté avec un formateur du Fablab dont je suis désolé d'avoir oublié le nom parce que j'aurais aimé lui faire une dédicace, nous avons réalisé que le problème disparaissait si nous exportions le dessin en .svg depuis Inkscape, plutôt qu'avec Illustrator. Après avoir résolu ce problème, nous avons enfin pu lancer la découpe qui s'est passée sans accroc et dont le résultat est assez chouette!

![](images/module4/ResultCompressed.jpg)

Les fichiers de design sont sur mon GitLab sous le nom de "OldKeepCalm.svg" pour le premier design et "NewKeepCalm.svg" pour le deuxième.


## Bilan de fin de module

Concluons ce module en vérifiant que la [checklist de tâches](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/4) a bien été suivie 

1. Expliquer ce que tu as appris en utilisant la découpeuse laser. ✔️
2. Expliquer comment tu as designer ton fichier CAD. ✔️
3. Documenter comment tu as fait ton objet. ✔️
4. Inclure les fichiers de design. ✔️