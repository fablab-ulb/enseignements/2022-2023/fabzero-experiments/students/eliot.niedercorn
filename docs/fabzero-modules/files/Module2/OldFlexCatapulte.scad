/*
* "FlexCatapult"
* Copyright (c) [2023], Eliot Niedercorn
*
* Ce projet est sous licence Creative Commons Attribution-ShareAlike [Attribution-ShareAlike 4.0 International]
* Pour voir une copie de cette licence, visitez https://creativecommons.org/licenses/by-sa/4.0/legalcode
*/


// General Parameters
$fn = 100;
hole_radius = 2.4;
hole_separation = 3.2;
rounding_power = 0.5;  // radius of the cylinder use for rounding my shape with minkowski()


// Catapult Part
catapult_num_holes = 3;
catapult_length = catapult_num_holes * (hole_radius + 3);
catapult_width = 5;
catapult_height = 1;
catapult_size = [catapult_length, catapult_width, catapult_height];

module catapult(catapult_size, rounding_power){
    minkowski(){
        cube(catapult_size);
        cylinder(r = rounding_power, h = catapult_height);
    }
}


module catapult_holes(catapult_size, catapult_num_holes, hole_radius, hole_separation){
    translate([3, catapult_size[1] / 2, -1])
    for (i = [0:catapult_num_holes-1]){
        translate([i * hole_separation, 0, 0])
        cylinder(r = hole_radius, h = catapult_size[2] + 3);
    }
}

difference () {
    catapult(catapult_size, rounding_power);
    catapult_holes(catapult_size, catapult_num_holes, hole_radius, hole_separation);
}


// Flexible Part
flexible_length = 40;
flexible_width = 1;
flexible_height = 2;
flexible_size = [flexible_length, flexible_width, flexible_height];

module flexible_part(catapult_size, flexible_size) {
    translate([catapult_size[0], catapult_size[1]/2 - 0.5, 0])
    cube(flexible_size);
}


// Base Part
base_length = 3;
base_width = 10;
base_height = 1;
base_size = [base_length, base_width, base_height];

module base_part(catapult_size, flexible_size, base_size){
    width_translation = (base_size[1] - catapult_size[1]) / 2;
    translate([catapult_size[0] + flexible_size[0], - width_translation, 0])
    cube(base_size);
}


// Support Part
angle = 20;
support_length = 2;
support_width = 15;
support_height = 1;
support_size = [support_length, support_width, support_height];

module support_part(catapult_size, flexible_size, base_size, support_size){
    translate([catapult_size[0] + flexible_size[0], base_size[1] / 2 - 1, 0])
    rotate(angle)
    cube(support_size);
}



// Support Holes
support_num_holes = 3;

module support_holes(catapult_size, flexible_size, base_size, support_size, support_num_holes, hole_radius, hole_separation){
    translate([catapult_size[0] + flexible_size[0] - 1.5, base_size[1] / 2 + 6, -1])
    rotate(angle + 90)
    for (i = [0:support_num_holes-1]){
        translate([i * hole_separation, 0, 0])
        cylinder(r = hole_radius, h = catapult_size[2] + 3);
    }
}


// Remove extra material for 3D printing
remove_angle = 9;
remove_length = 2.3;
remove_width =10;
remove_height = 10;
remove_size = [remove_length,remove_width, remove_height];

module remove_material(catapult_size, flexible_size, base_size, support_size){
    translate([catapult_size[0] + flexible_size[0] + 0.3, -1.4, -5])
    rotate(remove_angle)
    minkowski(){
        cube(remove_size);
        cylinder(r = rounding_power, h = catapult_height);
    }
}

// Piece generation
difference(){
    difference(){
        union () {
            flexible_part(catapult_size, flexible_size);
            base_part(catapult_size, flexible_size, base_size);
            minkowski(){
                hull(){
                    base_part(catapult_size, flexible_size, base_size);
                    support_part(catapult_size, flexible_size, base_size, support_size); 
                }    
                cylinder(r = rounding_power, h = catapult_height);
            }
        }
        remove_material(catapult_size, flexible_size, base_size, support_size);
    }
    support_holes(catapult_size, flexible_size, base_size, support_size, support_num_holes, hole_radius, hole_separation);
}


