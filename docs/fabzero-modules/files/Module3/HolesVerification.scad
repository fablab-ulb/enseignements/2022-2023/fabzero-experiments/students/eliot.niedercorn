/*
* "HolesVerification"
* Copyright (c) [2023], Eliot Niedercorn
*
* Ce projet est sous licence Creative Commons Attribution-ShareAlike [Attribution-ShareAlike 4.0 International]
* Pour voir une copie de cette licence, visitez https://creativecommons.org/licenses/by-sa/4.0/legalcode
*/

$fn = 200;

cube_length = 45;
cube_width = 15;
cube_height = 5;
cube_size = [cube_length, cube_width, cube_height];


module holes(cube_width){
    hole_separation = 3.2;
    translate([hole_separation + 1, cube_width / 2, -1])
    for (i = [0:3]){  
        radius = 2.4 + i * 0.5;  // Goes from 2.4 to 2.55
        translate([i * (2* radius + hole_separation), 0, 0])
        cylinder(r = radius, h = 7);
    }
}


difference(){
    cube(cube_size);
    holes(cube_width);
}
